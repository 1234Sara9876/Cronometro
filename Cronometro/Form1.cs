﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cronometro
{
    public partial class FormCro : Form
    {
        private Cro tmx;
        bool stat = true;

        public FormCro()
        {
            InitializeComponent();
            tmx = new Cro();
           
        }

        private void FormCro_Load(object sender, EventArgs e)
        {
            lblTimer.Enabled = true;
            btnStart.Enabled = true;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            tmx.Start(stat);
            lblTimer.Text = tmx.Mostrou;
            btnStart.Enabled = false;
            btnStop.Enabled = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            tmx.Stop(stat);
            lblTimer.Text = tmx.Mostrou;
            btnReset.Enabled = true;
            btnStart.Enabled = true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            tmx.Reset(stat);
            lblTimer.Text = tmx.Mostrou;
            btnStart.Enabled = true;
        }
    }
}
