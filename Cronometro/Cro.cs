﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cronometro
{
    public class Cro
    {
        /// <summary>
        /// region atributos 
        /// </summary>
        private DateTime iniciou;
        private DateTime terminou;
        private string mostrou;
        private TimeSpan decorreu;


        /// <summary>
        /// region propriedades
        /// </summary>
        public DateTime Iniciou { get; set; }
        public DateTime Terminou { get; set; }
        public string Mostrou { get; set; }
        public TimeSpan Decorreu { get; set; }

        /// <summary>
        /// region métodos construtores
        /// </summary>
        public Cro()
        {
            iniciou = DateTime.MinValue;
            terminou = DateTime.MinValue;
            mostrou = "00:00:00";
            decorreu = TimeSpan.Zero;

        }//construtor default

        public Cro(DateTime iniciou, DateTime terminou, string mostrou, TimeSpan decorreu)
        {
            Iniciou = iniciou;
            Terminou = terminou;
            Mostrou = mostrou;
            Decorreu = decorreu;

        }//construtor por parâmetros

        public Cro(Cro timer) : this(timer.Iniciou, timer.Terminou, timer.Mostrou, timer.Decorreu) { }
        //construtor de cópia

        /// <summary>
        /// outros métodos
        /// </summary>
        public void Start(bool stat)
        {
            Iniciou = DateTime.Now;
            if (stat)
            {
                Decorreu = DateTime.Now - Iniciou;
                Mostrou = TimeSpan.Zero.ToString();
            }
        }
        public void Stop(bool stat)
        {
            stat = false;
            Terminou = DateTime.Now;
            Decorreu = Terminou - Iniciou;
            Mostrou = Decorreu.ToString();
        }

        public void Reset(bool stat)
        {
            if (stat)
            {
                stat = false;
                Decorreu = TimeSpan.Zero;
                Mostrou = Decorreu.ToString();
            }
            else
            {
                Decorreu = TimeSpan.Zero;
                Mostrou = Decorreu.ToString();
            }
        }

        public override string ToString()
        {
            return Decorreu.Hours.ToString("00") +
                ":" +
                Decorreu.Minutes.ToString("00") +
                ":" +
                Decorreu.Seconds.ToString("00") +
                ":" +
                Decorreu.Milliseconds.ToString();
        }
    }
}
